Categories:Time
License:GPLv3
Web Site:https://github.com/ligi/MaterialTeaTimer/blob/HEAD/README.md
Source Code:https://github.com/ligi/MaterialTeaTimer
Issue Tracker:https://github.com/ligi/MaterialTeaTimer/issues

Auto Name:Material Tea Timer
Summary:Time your tea with style
Description:
Material design themed tea timer.
.

Repo Type:git
Repo:https://github.com/ligi/MaterialTeaTimer

Build:1.0,1
    commit=1.0
    subdir=app
    gradle=yes

Build:1.2,12
    commit=1.2
    subdir=android
    gradle=prodNoFirebase
    prebuild=sed -i -e '/withFirebaseCompile/d' build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.2
Current Version Code:12
