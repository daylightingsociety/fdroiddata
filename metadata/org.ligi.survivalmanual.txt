Categories:Reading
License:GPLv3
Web Site:https://github.com/ligi/SurvivalManual/blob/HEAD/README.md
Source Code:https://github.com/ligi/SurvivalManual
Issue Tracker:https://github.com/ligi/SurvivalManual/issues

Auto Name:Survival Manual
Summary:Learn how to survive
Description:
Survival Manual based on the Army Field Manual 21-76 - fully working offline
.

Repo Type:git
Repo:https://github.com/ligi/SurvivalManual

Build:1.0,1
    commit=1.0
    subdir=app
    gradle=yes

Build:1.5,15
    commit=1.5
    subdir=android
    submodules=yes
    gradle=prod
    prebuild=sed -i '/play_services/d' build.gradle && \
        sed -i '/android-sdk-manager/d' build.gradle

Build:1.6,16
    commit=1.6
    subdir=android
    submodules=yes
    gradle=prodNoFirebase
    prebuild=sed -i -e '/play_services/d'  -e '/withFirebaseCompile/d' build.gradle && \
        sed -i '/android-sdk-manager/d' build.gradle

Build:1.7,17
    commit=1.7
    subdir=android
    submodules=yes
    gradle=prodNoFirebase
    prebuild=sed -i -e '/play_services/d'  -e '/withFirebaseCompile/d' build.gradle && \
        sed -i '/android-sdk-manager/d' build.gradle

Build:1.8,18
    commit=1.8
    subdir=android
    submodules=yes
    gradle=prodNoFirebase
    prebuild=sed -i -e '/play_services/d'  -e '/withFirebaseCompile/d' build.gradle && \
        sed -i '/android-sdk-manager/d' build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.8
Current Version Code:18
